package br.com.mauda.seminario.cientificos.model;

public enum SituacaoInscricaoEnum {
    DISPONIVEL(1L, "disponivel"),
    COMPRADO(2L, "comprado"),
    CHECKIN(3L, "checkIn");

    private Long id; 
    private String nome;

    SituacaoInscricaoEnum(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
