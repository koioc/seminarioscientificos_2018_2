package br.com.mauda.seminario.cientificos.model;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;
    private Estudante estudante;
    private Seminario seminario;
    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        this.seminario.getInscricoes().add(this);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public void setEstudante(Estudante estudante) {
        this.estudante = estudante;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public void setSeminario(Seminario seminario) {
        this.seminario = seminario;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public void setSituacao(SituacaoInscricaoEnum situacao) {
        this.situacao = situacao;
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.estudante = estudante;
        this.estudante.getInscricoes().add(this);

        this.direitoMaterial = direitoMaterial;
        this.situacao = SituacaoInscricaoEnum.COMPRADO;

    }

    public void realizarCheckIn() {
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
    }

}
