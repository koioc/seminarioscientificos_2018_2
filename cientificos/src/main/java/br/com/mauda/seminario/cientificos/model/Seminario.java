package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Seminario {

    private Long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private Date data;
    private Integer qtdInscricoes;

    private List<AreaCientifica> areasCientificas;
    private List<Professor> professores;
    private List<Inscricao> inscricoes = new ArrayList<>();

    /**
     *
     * @param instituicao
     */

    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdInscricoes) {
        this.areasCientificas = new ArrayList<>();
        this.professores = new ArrayList<>();

        this.areasCientificas.add(areaCientifica);
        this.professores.add(professor);
        professor.getSeminarios().add(this);

        for (int i = 0; i < qtdInscricoes; i++) {
            new Inscricao(this);
        }

        this.qtdInscricoes = qtdInscricoes;
    }

    /**
     *
     * @param professores
     */
    public void setProfessores(List<Professor> professores) {
        this.professores = professores;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public void setAreasCientificas(List<AreaCientifica> areasCientificas) {
        this.areasCientificas = areasCientificas;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public void setQtdInscricao(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public void setInscricoes(List<Inscricao> inscricoes) {
        this.inscricoes = inscricoes;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

}
